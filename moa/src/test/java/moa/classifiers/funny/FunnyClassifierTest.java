/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * NaiveBayesTest.java
 * Copyright (C) 2013 University of Waikato, Hamilton, New Zealand
 */
package moa.classifiers.funny;

import moa.classifiers.funny.FunnyClassifier;
import static org.junit.Assert.assertEquals;

import moa.core.Measurement;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the NaiveBayes classifier.
 * 
 * @author  fracpete (fracpete at waikato dot ac dot nz)
 * @version $Revision$
 */
public class FunnyClassifierTest {

  private FunnyClassifier funnyClassifier;

  @Before
  public void setUp() throws Exception {
    funnyClassifier = new FunnyClassifier();
  }

  @Test
  public void testMeasurements() {
    Measurement[] measurements = funnyClassifier.getModelMeasurementsImpl();
    assertEquals(10, measurements[0].getValue(), 0.0);
  }

  @Test
  public void testDescription() {
    StringBuilder builder = new StringBuilder();
    assertEquals("", builder.toString());
    funnyClassifier.getModelDescription(builder, 0);
    assertEquals("So funny!", builder.toString());
  }
}

